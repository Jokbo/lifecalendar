$(document).ready(function(){
	var totalWeeks = 4680;
	var weeksPerYear = 52;
	var rows = 90;
	var counter = 1;
	
	for(var i = 0;i <= rows;i++){
		if(i % 5 == 0){
			$(".calendar").append("<div class='row year row"+i+"'>"+i+"</div>");
		}else{
			$(".calendar").append("<div class='row year row"+i+"'></div>");
		}
		
		for(var j = 1;j <= weeksPerYear;j++){
			if(i == 0){
				$(".weekNumber").append("<span class='weekLegend'>"+j+"</span>");
			}
			$(".row"+i).append("<span class='week' name="+counter+"></span>");
			counter++;
		}
	}

	if(typeof localStorage.lifeCalendar != "undefined"){
		var age = localStorage.lifeCalendar;
		fillWeeks(age);
	}else{
		console.log("fail!"); //No local storage found
	}

	$("#save").click(function(){

		if ( $.trim( $('#age').val() ) == '' ){
			alert('Please fill in your age');
		}else if(isNaN($('#age').val())){
			alert('Please insert numbers');
		}else{
			localStorage.lifeCalendar = $('#age').val() * 52; //storing weeks
			fillWeeks($('#age').val() * 52);
		}
	});

	$(".week").click(function(){
		var weeks = $(this).attr("name");
		localStorage.lifeCalendar = weeks;
		fillWeeks(weeks);
	})


	function fillWeeks(weeks){
		$(".row .week").removeClass("weekLived");
		for(var i = 1;i <= weeks;i++){
			$("[name='"+i+"']").addClass("weekLived");
	}

	}
})

/*Copyright (c) 2016 Gásten Sénior Kalemba Sauzande*/